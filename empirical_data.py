import numpy as np
import matplotlib.pyplot as plt
from graphs import *
import pickle
import os

data = np.loadtxt("C:\Juan\Facultad\Doctorado\GCN\Empirical\AA.dat")

file_name = "C:\Juan\Facultad\Doctorado\GCN\Empirical\laser_15.pkl"

transition_net, patterns_lab, clustering_coef, out_st, time_app = trasitional_graph(data, stepSize = 1, emb_dim = 3)

graph = Graph(parameter = None, lyap_exp = 1,
              transition_graph = transition_net,  pattern_label = patterns_lab, 
              clustering_coeff = clustering_coef, out_strength = out_st, time_apparence = time_app)
 

with open(file_name, 'wb') as output:
    pickle.dump(graph, output, pickle.HIGHEST_PROTOCOL)

  



